from sklearn import datasets

iris = datasets.load_iris()
X_iris = iris.data
y_iris = iris.target

from sklearn.cluster import k_means

cluster_model = k_means(X_iris,3)
print(cluster_model[1])

