from sklearn import datasets
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import numpy as np

iris = datasets.load_iris()
X_iris, y_iris = iris.data[:,:2], iris.target # Choose the first two attributes
from sklearn.multiclass import OneVsRestClassifier # Use of a one vs rest classifier

#split the dataset into training set and test set using 30% of the observations as the test set
X_train, X_test, y_train, y_test = train_test_split(X_iris, y_iris, test_size=0.30)

from sklearn.linear_model import LinearRegression
clf = OneVsRestClassifier(LinearRegression()) #Least squares
clf.fit(X_train, y_train)
print("intercept", clf.intercept_) #intercept values of the classifier
print("coefficient",clf.coef_)



