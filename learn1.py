def print_scores(scores):
    print("Accuracy: ", scores)
    print("Accuracy mean: ", scores.mean())
    print("Accuracy std: ", scores.std())
    print()

from sklearn import datasets

iris = datasets.load_iris() # load the dataset
X_iris, y_iris = iris.data, iris.target # X_iris holds the data attributes, while y_iris hold the target attribute (iris type)

from sklearn.cross_validation import train_test_split
from sklearn import preprocessing

#split the set into training set and test set
X_train, X_test, y_train, y_test = train_test_split(X_iris,y_iris,test_size=0.25, random_state=33)

from sklearn.preprocessing import StandardScaler
scaler = preprocessing.StandardScaler().fit(X_train)

#Standarize the features
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

import matplotlib.pyplot as plt
# colors = ['red','greenyellow','blue']
# for i in range(len(colors)):
#     xs = X_train[:,0][y_train == i]
#     ys = X_train[:,1][y_train == i]
#     plt.scatter(xs,ys,c=colors[i])
# plt.show()

from sklearn.linear_model import LinearRegression
clf = LinearRegression()
clf.fit(X_train, y_train)

from sklearn import metrics
from sklearn.cross_validation import cross_val_score, KFold

#k-NN classifier
print("---KNN classifier for K=1")
from sklearn.neighbors import KNeighborsClassifier
clf = KNeighborsClassifier(1) #creates a classifier with k = 1
clf.fit(X_train, y_train) #fit the dataset in the classifier
cv = KFold(X_iris.shape[0], 5, shuffle=True)
scores = cross_val_score(clf, X_iris, y_iris, cv=cv)
print_scores(scores)

print("---KNN classifier for K=3")
from sklearn.neighbors import KNeighborsClassifier
clf = KNeighborsClassifier(3) #creates a classifier with k = 1
clf.fit(X_train, y_train) #fit the dataset in the classifier
cv = KFold(X_iris.shape[0], 5, shuffle=True)
scores = cross_val_score(clf, X_iris, y_iris, cv=cv)
print_scores(scores)

print("---KNN classifier for K=5")
from sklearn.neighbors import KNeighborsClassifier
clf = KNeighborsClassifier(5) #creates a classifier with k = 1
clf.fit(X_train, y_train) #fit the dataset in the classifier
cv = KFold(X_iris.shape[0], 5, shuffle=True)
scores = cross_val_score(clf, X_iris, y_iris, cv=cv)
print_scores(scores)

#Perceptron clf
from sklearn.linear_model import Perceptron
print("---Perceptron classifier")
clf = Perceptron()
clf.fit(X_train, y_train) #fit the dataset in the classifier
cv = KFold(X_iris.shape[0], 5, shuffle=True, random_state=33)
scores = cross_val_score(clf, X_iris, y_iris, cv=cv)
print_scores(scores)

#Least squares clf
print("---Least Squares classifier")
from sklearn.linear_model import LinearRegression
clf = LinearRegression()
clf.fit(X_train, y_train) #fit the dataset in the classifier
cv = KFold(X_iris.shape[0], 5, shuffle=True, random_state=33)
scores = cross_val_score(clf, X_iris, y_iris, cv=cv)
print_scores(scores)

from sklearn.neural_network import MLPClassifier
print("Neural Network")
clf = MLPClassifier()
clf.fit(X_train, y_train) #fit the dataset in the classifier
cv = KFold(X_iris.shape[0], 5, shuffle=True, random_state=33)
scores = cross_val_score(clf, X_iris, y_iris, cv=cv)
print_scores(scores)


